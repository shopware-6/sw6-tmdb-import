<?php declare(strict_types=1);

namespace JMSE\TmdbImport\Command;

use Shopware\Core\Defaults;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\Uuid\Uuid;
use Shopware\Core\Framework\Context;
use Symfony\Component\HttpFoundation\Request;
use Shopware\Core\Content\Media\File\FileFetcher;
use Shopware\Core\Content\Media\File\FileSaver;
use Symfony\Component\Console\Attribute\AsCommand;
#[AsCommand(name: 'tmdb:import:upcoming')]
class TmdbImportMovieCommand extends Command
{
    protected static $defaultName = 'tmdb:import:upcoming';

    protected EntityRepository $productRepository;
    protected EntityRepository $mediaFolderRepository;
    protected EntityRepository $mediaRepository;
    private FileFetcher $fileFetcher;
    private FileSaver $fileSaver;

    public function __construct(
        EntityRepository $productRepository,
        EntityRepository $mediaRepository,
        EntityRepository $mediaFolderRepository,
        FileFetcher      $fileFetcher,
        FileSaver        $fileSaver,
        string           $name = null
    )
    {
        parent::__construct($name);
        $this->productRepository = $productRepository;
        $this->mediaFolderRepository = $mediaFolderRepository;
        $this->mediaRepository = $mediaRepository;
        $this->fileFetcher = $fileFetcher;
        $this->fileSaver = $fileSaver;
    }

    public function fetchDataFromTmdb(int $page): array
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.themoviedb.org/3/movie/upcoming?include_adult=false&language=de-DE&page=" . $page . "&api_key=6b1c19ca3f0d24a5824bcc0a3d9a6a86");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($output, true);
        return $response;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $suffix = uniqid();
        $data = $this->fetchDataFromTmdb(1);
        $taxId = '018d610f30ad73c9aaa9235b751ba431';

        for ($i = 1; $i <= $data['total_pages']; $i++) {
            sleep(10);
            $data = $this->fetchDataFromTmdb($i);
            foreach ($data['results'] as $key => $movie) {
                $uuid = Uuid::randomHex();
                $product = [[
                    'id' => $uuid,
                    'name' => $movie['title'],
                    'taxId' => $taxId,
                    'translations' => [
                        Defaults::LANGUAGE_SYSTEM => [
                            'name' => $movie['title']
                        ]
                    ],
                    'stock' => 0,
                    'price' => [
                        [
                            'currencyId' => Defaults::CURRENCY,
                            'gross' => 10,
                            'net' => 10,
                            'linked' => true,
                        ]
                    ],
                    'description' => 'Hallo Welt',
                    'createdAt' => date('Y-m-d') . 'T' . date('H:i:s') . '+02:00',
                    'productNumber' => 'TMDB-' . $movie['id'],
                ]];
                $url = 'https://image.tmdb.org/t/p/original' . $movie['poster_path'];
                try {
                    $this->productRepository->create($product, Context::createDefaultContext());
                    $this->uploadMediaByUrl($uuid, $product, $movie['id'], $movie['title'], $url);
                } catch (\Exception $exception) {
                    var_dump($exception->getMessage());
                }
            }
        }

        // Poster name desc
        // TODO: Genre als Eigenschaft ?
        // TODO: Release als Eigenschaft ?
        // TODO Price ?

        $output->writeln('Let\'s see if it works!');
        return self::SUCCESS;
    }

    public function uploadMediaByUrl(string $productId, $product, int $tmdb, string $name, string $url)
    {
        $criteria = new Criteria();
        $context = Context::createDefaultContext();
        $criteria->addFilter(new EqualsFilter('name', 'TMDB'));

        $media = $this->mediaFolderRepository->search($criteria, $context);

        $folderId = Uuid::randomHex();
        $configFolderId = Uuid::randomHex();
        try {
            if ($media->getElements() === null) {
                $response = $this->mediaFolderRepository->create(
                    [
                        [
                            'id' => $folderId,
                            'name' => 'TMDB',
                            'parentId' => '018d610f30fc72969e4d9acf4b71c93e',
                            'configuration' => [
                                'id' => $configFolderId
                            ]
                        ]
                    ],
                    $context
                );
                $id = $folderId;
            } else {
                $id = $media->first()->getUniqueIdentifier();
                $this->uploadBulkImages($productId, $product, $tmdb, $name, $id, $url);
            }

        } catch (\Exception $exception) {
            var_dump($exception->getMessage());
        }
    }

    public function uploadBulkImages(string $productId, $product, int $tmdb, string $name, string $folder, string $url)
    {
        $context = Context::createDefaultContext();
        $mediaRepository = $this->mediaRepository;
        $mediaIds = [];
        $ext = pathinfo($url, PATHINFO_EXTENSION);
        $fileName = basename($url, $ext);
        $request = new Request(['extension' => $ext], ['url' => $url]);
        $tempFile = tempnam(sys_get_temp_dir(), '');
        $file = $this->fileFetcher->fetchFileFromURL($request, $tempFile);
        $explodedFileName = explode('.', $fileName);
        unset($explodedFileName[count($explodedFileName) - 1]);
        $fileName = $text = sha1($name);

        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('fileName', $fileName));
        $searchMedia = $mediaRepository
            ->search($criteria, $context)
            ->first();

        if (!$searchMedia) {
            $mediaId = Uuid::randomHex();
            $media = [
                [
                    'id' => $mediaId,
                    'fileSize' => $file->getFileSize(),
                    'fileName' => $fileName,
                    'mediaFolderId' => $folder,
                    'mimeType' => $file->getMimeType(),
                    'fileExtension' => $ext,
                ]
            ];

            $mediaId = $mediaRepository->create($media, Context::createDefaultContext())
                ->getEvents()
                ->getElements()[1]
                ->getIds()[0];
            if (is_array($mediaId)) {
                $mediaId = $mediaId['mediaId'];
            }

            try {
                $this->upload($file, $fileName, $mediaId);
            } catch (\Exception $exception) {
                $fileName = $fileName . $mediaId;
                $this->upload($file, $fileName, $mediaId);
            }
            array_push($mediaIds, [
                'id' => $mediaId,
                'pid' => $productId,
                'tmdb' => $tmdb,
                'title' => $fileName
            ]);
        }
        foreach ($mediaIds as $key => $media) {

            $response = $this->productRepository->upsert(
                [
                    [
                        'stock' => $product[array_key_first($product)]['stock'],
                        'price' => $product[array_key_first($product)]['price'],
                        'taxId' => $product[array_key_first($product)]['taxId'],
                        'productNumber' => $product[array_key_first($product)]['productNumber'],
                        'translations' => $product[array_key_first($product)]['translations'],
                        'id' => $media['pid'],
                        'cover' => [
                            'mediaId' => $media['id'],
                            'id' => $media['id']
                        ],
                        'media' => [

                        ],
                    ],
                ], $context
            );
        }


        return $mediaIds;
    }

    public function findProductById(string $id)
    {
        $criteria = new Criteria();
        $context = Context::createDefaultContext();
        $criteria->addFilter(new EqualsFilter('id', $id));

        $product = $this->productRepository->search($criteria, $context);

        return $product->first();
    }

    public function upload($file, $fileName, $mediaId)
    {
        $this->fileSaver->persistFileToMedia(
            $file,
            $fileName,
            $mediaId,
            Context::createDefaultContext()
        );
    }
}