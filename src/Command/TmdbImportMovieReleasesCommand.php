<?php declare(strict_types=1);

namespace JMSE\TmdbImport\Command;

use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Attribute\AsCommand;
#[AsCommand(name: 'tmdb:import:releases')]
class TmdbImportMovieReleasesCommand extends Command
{
    protected static $defaultName = 'tmdb:import:releases';

    public EntityRepository $productRepository;

    public function __construct(
        EntityRepository $productRepository,
        string           $name = null
    )
    {
        parent::__construct($name);
        $this->productRepository = $productRepository;
    }

    public function fetchDataFromTmdb(int $tmdb): array
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.themoviedb.org/3/movie/" . $tmdb . "/release_dates?&api_key=6b1c19ca3f0d24a5824bcc0a3d9a6a86");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($output, true);
        return $response;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $context = Context::createDefaultContext();
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('releaseDate', null));
        $products = $this->productRepository->search($criteria, $context);

        foreach ($products->getElements() as $key => $product) {
            $tmdb = (int)str_replace('TMDB-', '', $product->get('productNumber'));

            $data = $this->fetchDataFromTmdb($tmdb);
            foreach ($data['results'] as $k => $item) {
                foreach ($item['release_dates'] as $kk => $date) {
                    if ($date['type'] === 5) {
                        $this->productRepository->upsert([['id' => $product->getUniqueIdentifier(),'releaseDate' => $date['release_date']]], $context);
                    }
                }
            }
        }

        $output->writeln('Let\'s see if it works!');
        return self::SUCCESS;
    }
}