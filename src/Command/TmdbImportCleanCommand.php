<?php declare(strict_types=1);

namespace JMSE\TmdbImport\Command;

use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Sorting\FieldSorting;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Attribute\AsCommand;
#[AsCommand(name: 'tmdb:import:clean')]
class TmdbImportCleanCommand extends Command
{
    protected static $defaultName = 'tmdb:import:clean';

    public EntityRepository $productRepository;

    public function __construct(
        EntityRepository $productRepository,
        string           $name = null
    )
    {
        parent::__construct($name);
        $this->productRepository = $productRepository;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $context = Context::createDefaultContext();
        $criteria = new Criteria();
        $criteria->addSorting(new FieldSorting('createdAt', FieldSorting::DESCENDING));
        $products = $this->productRepository->search($criteria, $context);
        foreach ($products->getElements() as $key => $product) {
            if( $product->get('name') != null){
                if (preg_match('/[^\p{Latin}0-9€, :!.\–\…\’\`\'\-"§$?%&\/()=#|<>]/u', $product->get('name'))) {
                    if ($product->get('id') !== null) {
                        $this->productRepository->delete([['id' => $product->get('id')]], $context);
                    }

                }
            }
        }

        $output->writeln('Let\'s see if it works!');
        return self::SUCCESS;
    }
}