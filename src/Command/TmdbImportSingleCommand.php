<?php declare(strict_types=1);

namespace JMSE\TmdbImport\Command;

use Shopware\Core\Content\Media\File\FileFetcher;
use Shopware\Core\Content\Media\File\FileSaver;
use Shopware\Core\Defaults;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\Uuid\Uuid;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Symfony\Component\Console\Input\InputArgument;

#[AsCommand(name: 'tmdb:import:single')]
class TmdbImportSingleCommand extends Command
{
    protected static $defaultName = 'tmdb:import:single';

    protected EntityRepository $productRepository;

    protected EntityRepository $mediaFolderRepository;
    protected EntityRepository $mediaRepository;

    public InputInterface $input;

    public function fetchDataFromTmdb(string $id): array
    {
        $ch = curl_init();
        // append_to_response=credits
        curl_setopt($ch, CURLOPT_URL, "https://api.themoviedb.org/3/movie/" . $id . "?api_key=6b1c19ca3f0d24a5824bcc0a3d9a6a86&append_to_response=credits");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($output, true);
        return $response;
    }

    protected function configure(): void
    {
        $this
            ->addArgument('tmdb', InputArgument::REQUIRED, 'Who do you want to greet?');
    }

    public function __construct(
        EntityRepository $productRepository,
        EntityRepository $mediaRepository,
        EntityRepository $mediaFolderRepository,
        FileFetcher      $fileFetcher,
        FileSaver        $fileSaver,
        string           $name = null
    )
    {
        parent::__construct($name);
        $this->productRepository = $productRepository;
        $this->mediaRepository = $mediaRepository;
        $this->mediaFolderRepository = $mediaFolderRepository;
        $this->fileFetcher = $fileFetcher;
        $this->fileSaver = $fileSaver;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->input = $input;
        $context = Context::createDefaultContext();
        $movie = $this->fetchDataFromTmdb($input->getArgument('tmdb'));
        $output->writeln('Import TMDB item by id!' . $input->getArgument('tmdb'));
        $output->writeln('Movie with Name ' . $movie['title'] . ' found!');

        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('name', $movie['title']));

        $items = $this->productRepository->search($criteria, $context);

        if ($items->count() === 0) {

            $newProductId = Uuid::randomHex();
            // TODO load media cover
            $this->productRepository->create([[
                'id' => $newProductId,
                'name' => $movie['title'],
                'price' => [
                    [
                        'currencyId' => Defaults::CURRENCY,
                        'gross' => 20,
                        'net' => 17.5,
                        'linked' => true,
                    ]
                ],
                'taxId' => '018d610f30ad73c9aaa9235b751ba431',
                'productNumber' => Uuid::randomHex(),
                'stock' => 0
            ]], $context);
            $this->updateCover($newProductId);
        } else {
            $this->updateCover($items->first()->getUniqueIdentifier());
        }

        return self::SUCCESS;
    }

    public function updateCover(string $productId): void
    {
        $movie = $this->fetchDataFromTmdb($this->input->getArgument('tmdb'));
        $url = 'https://image.tmdb.org/t/p/original/'.$movie['poster_path'];
        $tmdb = $this->input->getArgument('tmdb');
        var_dump($productId);
        $this->uploadMediaByUrl($productId, $movie, $tmdb, $movie['title'], $url);
    }

    public function uploadMediaByUrl(string $productId, $product, string $tmdb, string $name, string $url)
    {
        $criteria = new Criteria();
        $context = Context::createDefaultContext();
        $criteria->addFilter(new EqualsFilter('name', 'TMDB'));

        $media = $this->mediaFolderRepository->search($criteria, $context);

        $folderId = Uuid::randomHex();
        $configFolderId = Uuid::randomHex();
        try {
            if ($media->getElements() === null) {
                $response = $this->mediaFolderRepository->create(
                    [
                        [
                            'id' => $folderId,
                            'name' => 'TMDB',
                            'parentId' => '018d610f30fc72969e4d9acf4b71c93e',
                            'configuration' => [
                                'id' => $configFolderId
                            ]
                        ]
                    ],
                    $context
                );
                $id = $folderId;
            } else {
                $id = $media->first()->getUniqueIdentifier();
                $this->uploadBulkImages($productId, $product, $tmdb, $name, $id, $url);
            }

        } catch (\Exception $exception) {
            var_dump($exception->getMessage());
        }
    }

    public function upload($file, $fileName, $mediaId)
    {
        $this->fileSaver->persistFileToMedia(
            $file,
            $fileName,
            $mediaId,
            Context::createDefaultContext()
        );
    }

    public function uploadBulkImages(string $productId, $product, string $tmdb, string $name, string $folder, string $url)
    {
        $context = Context::createDefaultContext();
        $mediaRepository = $this->mediaRepository;
        $mediaIds = [];
        $ext = pathinfo($url, PATHINFO_EXTENSION);
        $fileName = basename($url, $ext);
        $request = new Request(['extension' => $ext], ['url' => $url]);
        $tempFile = tempnam(sys_get_temp_dir(), '');
        $file = $this->fileFetcher->fetchFileFromURL($request, $tempFile);
        $explodedFileName = explode('.', $fileName);
        unset($explodedFileName[count($explodedFileName) - 1]);
        $fileName = $text = sha1($name);

        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('fileName', $fileName));
        $searchMedia = $mediaRepository
            ->search($criteria, $context)
            ->first();

            $mediaId = Uuid::randomHex();
            $media = [
                [
                    'id' => $mediaId,
                    'fileSize' => $file->getFileSize(),
                    'fileName' => $fileName,
                    'mediaFolderId' => $folder,
                    'mimeType' => $file->getMimeType(),
                    'fileExtension' => $ext,
                ]
            ];

            $mediaId = $mediaRepository->create($media, Context::createDefaultContext())
                ->getEvents()
                ->getElements()[1]
                ->getIds()[0];
            if (is_array($mediaId)) {
                $mediaId = $mediaId['mediaId'];
            }

            try {
                $this->upload($file, $fileName, $mediaId);
            } catch (\Exception $exception) {
                $fileName = $fileName . $mediaId;
                $this->upload($file, $fileName, $mediaId);
            }
            array_push($mediaIds, [
                'id' => $mediaId,
                'pid' => $productId,
                'tmdb' => $tmdb,
                'title' => $fileName
            ]);
        
        foreach ($mediaIds as $key => $media) {

            $response = $this->productRepository->upsert(
                [
                    [
                        'stock' => $product[array_key_first($product)]['stock'],
                        'price' => $product[array_key_first($product)]['price'],
                        'taxId' => $product[array_key_first($product)]['taxId'],
                        'productNumber' => $product[array_key_first($product)]['productNumber'],
                        'translations' => $product[array_key_first($product)]['translations'],
                        'id' => $media['pid'],
                        'cover' => [
                            'mediaId' => $media['id'],
                            'id' => $media['id']
                        ],
                        'media' => [

                        ],
                    ],
                ], $context
            );
        }


        return $mediaIds;
    }

}