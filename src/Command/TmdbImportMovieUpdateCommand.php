<?php declare(strict_types=1);

namespace JMSE\TmdbImport\Command;

use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\Uuid\Uuid;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Shopware\Core\Content\Product\SalesChannel\Sorting\ProductSortingEntity;
use Shopware\Core\Content\Product\SalesChannel\Sorting\ProductSortingCollection;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Sorting\FieldSorting;
use Symfony\Component\Console\Attribute\AsCommand;
#[AsCommand(name: 'tmdb:import:update')]
class TmdbImportMovieUpdateCommand extends Command
{
    protected static $defaultName = 'tmdb:import:update';
    public EntityRepository $productRepository;
    public EntityRepository $categoryRepository;
    public EntityRepository $propertyOptionRepository;
    public EntityRepository $propertyGroupRepository;
    public EntityRepository $productPropertyRepository;
    public EntityRepository $productConfiguratorSettingRepository;

    public function __construct(
        EntityRepository $productRepository,
        EntityRepository $categoryRepository,
        EntityRepository $propertyGroupRepository,
        EntityRepository $propertyOptionRepository,
        EntityRepository $productPropertyRepository,
        EntityRepository $productConfiguratorSettingRepository,
        string           $name = null
    )
    {
        parent::__construct($name);
        $this->productRepository = $productRepository;
        $this->propertyGroupRepository = $propertyGroupRepository;
        $this->categoryRepository = $categoryRepository;
        $this->propertyOptionRepository = $propertyOptionRepository;
        $this->productPropertyRepository = $productPropertyRepository;
        $this->productConfiguratorSettingRepository = $productConfiguratorSettingRepository;
    }

    public function fetchDataFromTmdb(string $id): array
    {
        $ch = curl_init();
        // append_to_response=credits
        curl_setopt($ch, CURLOPT_URL, "https://api.themoviedb.org/3/movie/" . $id . "?api_key=6b1c19ca3f0d24a5824bcc0a3d9a6a86&append_to_response=credits");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($output, true);
        return $response;
    }

    public function createGenres(int $id, array $genres): array
    {
        $tmp = [];
        foreach ($genres as $key => $genre) {
            $context = Context::createDefaultContext();
            $criteria = new Criteria();
            $criteria->addFilter(new EqualsFilter('name', $genre['name']));
            $categories = $this->categoryRepository->search($criteria, $context);
            if ($categories->count() === 0) {
                $uuid = Uuid::fromStringToHex((string)$genre['id']);
                $this->categoryRepository->create([[
                    'id' => $uuid,
                    'name' => $genre['name'],
                    'active' => true,
                    'parentId' => '018dbc147f37778daf75efafc9c0ad33'
                ]], $context);
            } else {
                $uuid = $categories->first()->getUniqueIdentifier();
            }
            $tmp[] = ['id' => $uuid];
        }
        return $tmp;
    }

    public function createActors(int $id, array $crew): array
    {
        $context = Context::createDefaultContext();
        $actors = [];

        foreach ($crew as $key => $actor) {
            if ($actor['known_for_department'] == 'Acting') {
                $criteria = new Criteria();
                $criteria->addFilter(new EqualsFilter('name', 'Actor'));
                $search = $this->propertyGroupRepository->search($criteria, $context);

                if ($search->count() === 0) {
                    $id = Uuid::randomHex();
                    $this->propertyGroupRepository->create([[
                        'id' => $id,
                        'name' => 'Actor'
                    ]], $context);
                } else {
                    $id = $search->first()->getUniqueIdentifier();
                }

                // TODO: upload image of actor
                $criteria2 = new Criteria();
                $criteria2->addFilter(new EqualsFilter('name', $actor['name']));
                $criteria2->addFilter(new EqualsFilter('groupId', $id));
                $search2 = $this->propertyOptionRepository->search($criteria2, $context);

                if ($search2->count() === 0) {
                    $id2 = Uuid::randomHex();
                    $this->propertyOptionRepository->create([[
                        'id' => $id2,
                        'groupId' => $id,
                        'translations' => [
                            '2fbb5fe2e29a4d70aa5854ce7ce3e20b' => [
                                'name' => $actor['name']
                            ]
                        ]
                    ]], $context);
                    $actors[] = [
                        'optionId' => $id2,
                        'groupId' => $id,
                        'translations' => [
                            '2fbb5fe2e29a4d70aa5854ce7ce3e20b' => [
                                'name' => $actor['name']
                            ]
                        ]
                    ];
                } else {
                    $actors[] = [
                        'optionId' => $search2->first()->getUniqueIdentifier(),
                        'groupId' => $id,
                        'translations' => [
                            '2fbb5fe2e29a4d70aa5854ce7ce3e20b' => [
                                'name' => $actor['name']
                            ]
                        ]
                    ];
                }
            }
        }
        return $actors;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $context = Context::createDefaultContext();
        $criteria = new Criteria();
        // $criteria->addFilter(new EqualsFilter('releaseDate', null));
        $criteria->addSorting(new FieldSorting('updatedAt', FieldSorting::ASCENDING));
        $products = $this->productRepository->search($criteria, $context);

        foreach ($products->getElements() as $key => $element) {
            $id = str_replace('TMDB-', '', $element->get('productNumber'));
            $movie = $this->fetchDataFromTmdb($id);

            if ($movie['credits'] !== null) {
                $actors = $this->createActors((int)$movie['id'], $movie['credits']['crew']);
            } else {
                $actors = [];
            }

            if ($movie['genres'] !== null) {
                $genreIds = $this->createGenres((int)$movie['id'], $movie['genres']);
            } else {
                $genreIds = [];
            }

            $this->productRepository->upsert([[
                'id' => $element->getUniqueIdentifier(),
                'description' => $movie['overview'],
                'releaseDate' => $movie['release_date'],
                'categories' => $genreIds,
                'properties' => $actors
            ]
            ], $context);

            // $this->prepareProductConfiguratorSettings($element->getUniqueIdentifier(), $actors);
            // $this->removeDublicates();
            sleep(5);
        }

        $output->writeln('Let\'s see if it works!');
        return self::SUCCESS;
    }

    public function removeDublicates()
    {
        $context = Context::createDefaultContext();
        $criteria = new Criteria();
        $props = $this->propertyOptionRepository->search($criteria, $context);
        foreach ($props as $key => $prop) {
            try {
                $this->propertyOptionRepository->delete([['id' => $prop->getUniqueIdentifier()]],$context);
            }catch (\Exception $exception){}
        }
    }

    public function prepareProductConfiguratorSettings($id, $actors)
    {
        $context = Context::createDefaultContext();

        foreach ($actors as $key => $item) {
            $this->productConfiguratorSettingRepository->create([[
                'productId' => $id,
                'groupId' => $item['groupId'],
                'optionId' => $item['optionId']
            ]], $context);
        }
    }
}